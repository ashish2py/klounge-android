package com.klounge.kloungefeedback;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Klounge extends Activity {
	
	Button btnNext;
	EditText branchCode;
	int brCode;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final SharedPreferences prefs = getApplicationContext().getSharedPreferences("K-LOUNGE",getApplicationContext().MODE_PRIVATE);
		final SharedPreferences.Editor edit = prefs.edit();
		Integer barCode = prefs.getInt("barCode", 0);
		
		//check shared preference is null or exist ?
		if(barCode == 0){

			setContentView(R.layout.activity_klounge);
			
			btnNext = (Button)findViewById(R.id.btnNext);
			btnNext.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					//Write a Code to Save Branch Code
					branchCode = (EditText)findViewById(R.id.branchCode);
					
					if(branchCode.getText().length() !=0){
						//brCode = Integer.parseInt(branchCode.getText().toString());
						brCode= Integer.parseInt(branchCode.getText().toString());
						
						Toast.makeText(getApplicationContext(), "Wait..." + brCode,Toast.LENGTH_LONG).show();
						edit.putInt("barCode", brCode);
						edit.commit();
						
						//Code That starts next screen
						Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
						startActivity(intent);									
						
						
						
					}else{
						branchCode.setError("Please Enter Branch Code");
						Toast.makeText(getApplicationContext(), "Oops ! please enter the branch code",Toast.LENGTH_LONG).show();
					}	
				}
			});
		}else{
			//Code That starts next screen
			Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
			startActivity(intent);			
		}
		
	}

}

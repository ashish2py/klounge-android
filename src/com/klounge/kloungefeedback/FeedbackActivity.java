package com.klounge.kloungefeedback;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class FeedbackActivity extends Activity {
	EditText personName, personContact;
	String name, comment, smileStatus, negativeFeed;
	int contact;
	float feedRating;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);

		ImageView happy = (ImageView) findViewById(R.id.happyDrawable);
		ImageView sad = (ImageView) findViewById(R.id.sadDrawable);
		final RatingBar ratingBar = (RatingBar) findViewById(R.id.happySmileyRating);
		final EditText sadComment = (EditText) findViewById(R.id.sadSmileyComment);

		personName = (EditText) findViewById(R.id.personName);
		personContact = (EditText) findViewById(R.id.personContact);

		final TextView feedbackStatus = (TextView) findViewById(R.id.choiceStatus);
		final TextView branchCode = (TextView) findViewById(R.id.branchCode);
		final TextView ratingValue = (TextView) findViewById(R.id.ratingValue);
		final Button submitFeedbackBtn = (Button) findViewById(R.id.submitFeedbackBtn);
		// shared preference
		SharedPreferences prefs = getApplicationContext().getSharedPreferences(
				"K-LOUNGE", getApplicationContext().MODE_PRIVATE);
		Integer barCode = prefs.getInt("barCode", 0);
		branchCode.setText("Branch ID : " + barCode);

		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				ratingValue.setText(String.valueOf(rating));
				feedRating = rating;
			}
		});

		// happy image click method
		happy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ratingBar.setVisibility(View.VISIBLE);
				sadComment.setVisibility(View.GONE);
				smileStatus = "POSITIVE";
				feedbackStatus.setText(smileStatus);
				ratingValue.setVisibility(View.VISIBLE);
				if (sadComment.getText().length() != 0) {
					sadComment.setText(""); // clear the form
					negativeFeed = sadComment.getText().toString(); // set empty
																	// string
				}
				// Toast.makeText(getApplicationContext(), "happy" +
				// ratingBar.getRating(),Toast.LENGTH_LONG).show();
			}
		});

		// sad image click method
		sad.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ratingBar.setVisibility(View.GONE);
				ratingValue.setVisibility(View.GONE);
				smileStatus = "NEGATIVE";
				sadComment.setVisibility(View.VISIBLE);
				feedbackStatus.setText(smileStatus);
				feedRating = 0;
				// Toast.makeText(getApplicationContext(),
				// "sad",Toast.LENGTH_LONG).show();
			}
		});

		// submitting form and new screen launch goes her ...
		submitFeedbackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// check whether form is valid or not ?
				if (validateForm()) {

					// fetching the form data and making an string .
					name = personName.getText().toString();
					contact = Integer.parseInt(personContact.getText()
							.toString());

					if (sadComment.getText().length() != 0) {
						negativeFeed = sadComment.getText().toString();
					}
					// server sending part goes here ........
					Toast.makeText(
							getApplicationContext(),
							"name :" + name + "\n contact :" + contact
									+ "\n rating :" + feedRating
									+ "\n feedback :" + smileStatus
									+ "\n negative feed : " + negativeFeed,
							Toast.LENGTH_LONG).show();

					//open the facebook page as an intent
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://www.facebook.com/ashish2py"));
					startActivity(intent);

				} else {
					// Toast.makeText(getApplicationContext(),
					// "found new error... we are looking into it.",Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	// Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(1, 1, 0, "Settings");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 1: // my list
			Intent settingIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}// end of menu-item click listener

	public Boolean validateForm() {
		if (personName.getText().length() == 0) {
			personName.setError("Please Enter Your Name");
			return false;
		} else if (personContact.getText().length() == 0) {
			personContact.setError("Please Enter Contact Number");
			return false;
		}
		return true;
	}

}
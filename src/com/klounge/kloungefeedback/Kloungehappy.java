package com.klounge.kloungefeedback;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.transform.Result;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.Toast;

public class Kloungehappy extends Activity {
	
	private static final String Context = null;
	ImageView imgHappy, imgSad;
	RatingBar ratingBar;
	String ratingValue;
	int brnchCode;
	EditText edtName, edtPhone, edtStatus, edtComments;
	Button btnGetLucky;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kloungehappy);
		
				Klounge kl = new Klounge();
				brnchCode = kl.brCode;
				
				SharedPreferences shared = getSharedPreferences(Context, MODE_PRIVATE);
				int channel = shared.getInt(getString(R.string.branch_code), brnchCode);
				Toast.makeText(getApplicationContext(), channel, Toast.LENGTH_LONG).show();
				
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						imgHappy = (ImageView)findViewById(R.id.imageView1);
						imgSad = (ImageView)findViewById(R.id.imageView2);
						
						ratingBar = (RatingBar)findViewById(R.id.ratingBar1);
						
						edtName = (EditText)findViewById(R.id.editText1);
						edtPhone = (EditText)findViewById(R.id.editText2);
						edtStatus = (EditText)findViewById(R.id.editText3);
						edtComments = (EditText)findViewById(R.id.editText4);
						
						btnGetLucky = (Button)findViewById(R.id.btnGetLucky);
					}
				});
				
		
		
		//Code for what happens on clicking Happy Smiley
		imgHappy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				//THREAD FOR HAPPY SMILEY
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						edtStatus.setText("HAPPY");
						ratingBar.setVisibility(View.VISIBLE);
						edtComments.setVisibility(View.INVISIBLE);
					}
				});
				//THREAD ENDS HERE
				
			}
		});
		
		//Code for what happens on clicking Sad Smiley
		imgSad.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				//THREAD FOR UNHAPPY SMILEY
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						edtStatus.setText("UNHAPPY");
						ratingBar.setVisibility(View.INVISIBLE);
						edtComments.setVisibility(View.VISIBLE);
					}
				});
				//THREEAD ENDS HERE
			}
		});
		
		//Code for GetLucky button
		btnGetLucky.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//THREAD FOR BUTTON CLICK (GET LUCKY)
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//
						//
						//Collect Data From Form Fields
						String rating = ratingValue;
						String name = edtName.getText().toString();
						String phone = edtPhone.getText().toString();
						String status = edtStatus.getText().toString();
						String comments = edtComments.getText().toString();
						
//						Make a server Connection using AsynTask
						ConnectivityManager connMgr = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
						
						NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
						
						if (networkInfo != null && networkInfo.isConnected()) {
							new DownloadWebpageText().execute("Will Do Later!!!");
//							e.g.www.somedomain.com/db.php?field_rating=rating&field_name=name&...
//							its name=value pair
						} else {
							Toast.makeText(getApplicationContext(), "No Internet", Toast.LENGTH_LONG).show();
						}
						
						Toast.makeText(getApplicationContext(), "Thanks for experience…", Toast.LENGTH_LONG).show();

					}
				});
				
				
			}
		});
		
		//Code for what happens on rating selection
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			
			@Override
			public void onRatingChanged(RatingBar arg0, float arg1, boolean arg2) {
				// TODO Auto-generated method stub
				ratingValue = String.valueOf(arg1);
			}
		});
	}
	
	private class DownloadWebpageText extends AsyncTask<String, String, Result>{

		@Override
		protected Result doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			//Create a HTML connection and perform DB operation using PHP on serverside
			//Enter proper URL below
			URL url=null;
			try {
				url = new URL(params[0]);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return null;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.kloungehappy, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.action_settings:
	        	Intent intent = new Intent(this, SettingsActivity.class);
	        	startActivity(intent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

}

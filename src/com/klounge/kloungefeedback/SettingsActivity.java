package com.klounge.kloungefeedback;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		final TextView branchCode = (TextView) findViewById(R.id.branchCode);
		final SharedPreferences prefs = getApplicationContext().getSharedPreferences("K-LOUNGE", getApplicationContext().MODE_PRIVATE);
	    final SharedPreferences.Editor editor = prefs.edit();

		Button clearBCodeBtn = (Button) findViewById(R.id.clearBranchCodeBtn);
		Integer barCode = prefs.getInt("barCode", 0);
		branchCode.setText("Branch ID : " + barCode);
		
		//clear branch code and save new one.
		clearBCodeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {				
			    editor.remove("barCode");
			    editor.commit(); 
			    Intent loginIntent = new Intent(getApplicationContext(),Klounge.class);
			    startActivity(loginIntent);
			    //finish();				
			}
		});		
	}
}
